# TP k8s de Sylvain Reynaud

## Application

Afin de suivre les 12 factors, l'application est stateless et log sur la sortie standard.

L'application s'arrête de façon gracieuse avec les signaux système `SIGINT`, `SIGTERM` et `SIGHUP`.

L'application dispose d'un health check grâce au package npm `express-actuator`.

## Gitlab CI

Kaniko est utilisé pour le build des images Docker. Kaniko résout deux problèmes liés au Docker-in-Docker :

- Dind nécessite le privileged mode pour fonctionner, ce qui constitue un problème de sécurité important,
- Dind entraîne généralement une pénalité de performance et peut être assez lent.

### Build stage et push stage

Pour les stages de build et de push je me base sur la [documentation de Gitlab](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html#building-a-docker-image-with-kaniko)

```yaml
docker-build:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
  rules:
    - if: $CI_COMMIT_BRANCH
      exists:
        - Dockerfile
```

Le build se lance seulement si la branche du commit contient un Dockerfile.

Dans `Settings` > `CI/CD` > `Variables`, les variables suivantes sont redéfinies :

- `CI_REGISTRY` : `docker.io`,
- `CI_REGISTRY_USER` : `{Dockerhub username}`,
- `CI_REGISTRY_PASSWORD` : `{Dockerhub password}` comme variable masquée,
- `CI_REGISTRY_IMAGE` : `index.docker.io/sylvaindockersupercool/buteur`

[Documentation des variables CI](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)

### Deploy stage

On ajoute le fichier KUBECONFIG (le yaml dans ~/.kube, voire [partie Kubernetes](#Kubernetes)) dans `Settings` > `CI/CD` > `Variables`.

L'image `dtzar/helm-kubectl` contient kubectl pour executer les fichiers yaml.

```yaml
deploy:
  stage: deploy
  image: dtzar/helm-kubectl
  script:
    - kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.48.1/deploy/static/provider/do/deploy.yaml
    - kubectl apply -f k8s
```

## Kubernetes

Afin de développer et tester cette solution, j'utilise un [cluster k8s sur DitigalOcean](https://cloud.digitalocean.com/kubernetes/clusters).

[Installation de kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/)

Installation de doctl :

```shell=
sudo snap install doctl
sudo snap connect doctl:kube-config
doctl auth init
```

Je télécharge le config file puis je le mets dans `~/.kube` et je teste :
`cd ~/.kube && kubectl --kubeconfig="k8s-1-21-2-do-2-fra1-1627259075949-kubeconfig.yaml" get nodes`

```shell=
NAME            STATUS   ROLES    AGE   VERSION
pool-tp-85gij   Ready    <none>   93s   v1.21.2
```

Création d'un namespace `tp` : `kubectl create namespace tp`

Je place tous mes fichiers yaml dans `k8s/` : [configmap.yml](./k8s/configmap.yml), [pod.yml](./k8s/pod.yml), [service.yml](./k8s/service.yml), [ingress.yml](./k8s/ingress.yml)

La configmap contient le nom et la photo (en base64) du buteur.

Le pod contient l'image Docker et est lié avec la configmap.

Etant donné que l'application exposé sur le port 3000, le service expose le pod sur le port 3000.

L'application est finalement exposé sur un Ingress ngnix avec le nom de domaine `do.exam.local`.

J'applique l'Ingress Controller : `kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.48.1/deploy/static/provider/do/deploy.yaml`.

J'applique mes fichiers yaml: `kubectl apply -f k8s`.

Je regarde si l'Ingress a bien une IP :

```shell=
➜  26-mai-93-k8s git:(main) ✗ kubectl get ingress
NAME         CLASS    HOSTS           ADDRESS         PORTS   AGE
ingress-tp   <none>   do.exam.local   157.245.18.10   80      89s
```

J'ajoute `157.245.18.10 do.exam.local` (l'IP retourné dans la commande précédante) à `/etc/hosts`, et voilà :

![Preuve](./images/proof.png)
